// swift-tools-version:5.3
import PackageDescription

let version = "1.0.9"
let path = "release"

let package = Package(
    name: "VibeBuildXCFramework",
    products: [
        .library(
            name: "GoogleMapsBase",
            targets: [
                "GoogleMapsBase"
            ]
        ),
        .library(
            name: "GoogleMapsCore",
            targets: [
                "GoogleMapsCore"
            ]
        ),
        .library(
            name: "GoogleMaps",
            targets: [
                "GoogleMaps",
                "GoogleMapsBase",
                "GoogleMapsCore"
            ]
        ),
        .library(
            name: "GoogleMapsM4B",
            targets: [
                "GoogleMapsM4B"
            ]
        ),
        .library(
            name: "GooglePlaces",
            targets: [
                "GooglePlaces",
                "GoogleMapsBase"
            ]
        ),
        .library(
            name: "AppAuth",
            targets: [
                "AppAuth"
            ]
        ),
        .library(
            name: "GoogleSignIn",
            targets: [
                "GoogleSignIn"
            ]
        ),
        .library(
            name: "GTMAppAuth",
            targets: [
                "GTMAppAuth"
            ]
        ),
        .library(
            name: "GTMSessionFetcher",
            targets: [
                "GTMSessionFetcher"
            ]
        )
    ],
    targets: [
        .binaryTarget(
            name: "GoogleMaps",
            path: "artifact/\(path)/\(version)/GoogleMaps.xcframework"
        ),
        .binaryTarget(
            name: "GoogleMapsBase",
            path: "artifact/\(path)/\(version)/GoogleMapsBase.xcframework"
        ),
        .binaryTarget(
            name: "GoogleMapsCore",
            path: "artifact/\(path)/\(version)/GoogleMapsCore.xcframework"
        ),
        .binaryTarget(
            name: "GoogleMapsM4B",
            path: "artifact/\(path)/\(version)/GoogleMapsM4B.xcframework"
        ),
        .binaryTarget(
            name: "GooglePlaces",
            path: "artifact/\(path)/\(version)/GooglePlaces.xcframework"
        ),
        .binaryTarget(
            name: "AppAuth",
            path: "artifact/\(path)/\(version)/AppAuth.xcframework"
        ),
        .binaryTarget(
            name: "GoogleSignIn",
            path: "artifact/\(path)/\(version)/GoogleSignIn.xcframework"
        ),
        .binaryTarget(
            name: "GTMAppAuth",
            path: "artifact/\(path)/\(version)/GTMAppAuth.xcframework"
        ),
        .binaryTarget(
            name: "GTMSessionFetcher",
            path: "artifact/\(path)/\(version)/GTMSessionFetcher.xcframework"
        ),
    ]
)
