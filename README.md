# Google Maps Swift Package
## Requirements
* [iOS 10.0](https://wikipedia.org/wiki/IOS_10) or later.
* [Xcode 13.0](https://developer.apple.com/xcode) or later.

## Installation
- Add the following dependency to your project's `Package.swift`.

```swift
dependencies: [
    .package(url: "https://gitlab.com/tuongdang/vibe-ios-build-xcframework.git", .upToNextMinor(from: "1.0.0"))
]
```
### Known Issues
- If you use a Google Maps Swift package in an app with extensions, the build system incorrectly embeds the binary dependencies alongside the extension in the PlugIns directory, causing validation of the archived app to fail. (69834549) (FB8761306)

    **Workaround:** Add a scheme post-build action which removes the embedded binaries from the PlugIns directory after the build, e.g. `rm -rf "${TARGET_BUILD_DIR}/${TARGET_NAME}.app"/PlugIns/*.framework`.

# Process Release a new version

## 1: Update version of (GoogleMap) on file Cartfile
- should remove folder carthage in project.
## 2: Run script make_xcframework.sh
- waiting fetch all framework and build to file .xcframework
## 3: Update checksum and framework
- Go to folder Build open file checksum and copy each one shasum of framework and update it to file Package.swift
- Go to folder release/download create a folder with name of version release eg<1.5.0>
- Go to folder Build copy all framework and paste to folder release above.
## 4: Commit and push all
- git add .
- git commit -m "Update vibe ios build common version ...."
- git push origin master
## 5: Release and public a new version on gitlab
- Go to repo https://gitlab.com/tuongdang/vibe-ios-build-common-xcframework
- Setting:  Deployments/releases 
- Create new version release for it.